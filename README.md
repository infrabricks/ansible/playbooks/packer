# Packer

Ansible playbook to install Packer.

## Requirements

* You need this role to use this playbook : [Packer](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/packer)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Install Packer
  hosts: all
  tasks:
    - name: Include Packer role
      ansible.builtin.include_role:
        name: packer
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
